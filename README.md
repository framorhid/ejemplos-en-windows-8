Ejemplos para el CEDV (2014-2015) de Ogre3D 1.8 con CEGUI 0.76 (ó 0.76), Bullet 2.76 (creo) y OgreBullet montados en Visual C++ 2010 Express y siguiendo los siguientes tutoriales:

http://www.ogre3d.org/tikiwiki/Setting+Up+An+Application+-+Visual+Studio
https://www.youtube.com/watch?v=nWm5aec8xkE

http://www.ogre3d.org/tikiwiki/tiki-index.php?page=OgreBullet+Tutorial+1



Nota: En los ejemplos dónde no se usa CEGUI creo que se produce una violación de segmento en la que estoy trabajando

Nota 2: No hay que olvidad que las rutas de los includes y los lib son los de MI PC, por lo que si compilan los ejemplos tal y como están es posible que haya errores.

Nota 3: La mayoría de ejemplos estan preparados para compilar en modo Debug.