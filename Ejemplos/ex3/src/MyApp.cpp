/*********************************************************************
 * Módulo 2. Curso de Experto en Desarrollo de Videojuegos
 * Autor: Carlos González Morcillo     Carlos.Gonzalez@uclm.es
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#include "MyApp.h" 

MyApp::MyApp() {
  _sceneManager = NULL;
  _framelistener = NULL;
}

MyApp::~MyApp() {
  //  delete _root;
  //  delete _framelistener;
}

int MyApp::start() {
  _root = new Ogre::Root();
  
  if(!_root->restoreConfig()) {
    _root->showConfigDialog();
    _root->saveConfig();
  }
  
  Ogre::RenderWindow* window = _root->initialise(true,"MyApp Example");
  _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC);
  
  _cam = _sceneManager->createCamera("MainCamera");
  _cam->setPosition(Ogre::Vector3(5,20,20));
  _cam->lookAt(Ogre::Vector3(0,0,0));
  _cam->setNearClipDistance(5);
  _cam->setFarClipDistance(10000);

  _camBack = _sceneManager->createCamera("BackCamera");
  _camBack->setPosition(Ogre::Vector3(-5,-20,20));
  _camBack->lookAt(Ogre::Vector3(0,0,0));
  _camBack->setNearClipDistance(5);
  _camBack->setFarClipDistance(10000);
  
  Ogre::Viewport* viewport = window->addViewport(_cam);
  viewport->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0));
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  _cam->setAspectRatio(width / height);
  _camBack->setAspectRatio(width / height);
  
  
  loadResources();
  createScene();
  
  Ogre::SceneNode *node = _sceneManager->getSceneNode("SinbadNode");
  
  _framelistener = new MyFrameListener(window, _cam, node);
  _root->addFrameListener(_framelistener);
  
  createGUI();

  _root->startRendering();

  return 0;
}

void MyApp::loadResources() {
  Ogre::ConfigFile cf;
  cf.load("resources.cfg");
  
  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;    datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation
            (datastr, typestr, sectionstr);	
    }
  }
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void MyApp::createScene() {
  Ogre::Entity* ent1 = _sceneManager->createEntity("Sinbad.mesh");
  Ogre::SceneNode* node1 = _sceneManager->createSceneNode("SinbadNode");
  node1->attachObject(ent1);
  _sceneManager->getRootSceneNode()->addChild(node1);


}

void MyApp::createGUI()
{
  //CEGUI
  renderer = &CEGUI::OgreRenderer::bootstrapSystem();
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
  CEGUI::System::getSingleton().setDefaultFont("DejaVuSans-10");
  CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");

  //Sheet
  CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Ex1/Sheet");

  //Render Texture - to - target
  Ogre::TexturePtr tex = _root->getTextureManager()->createManual(
								  "RTT",
								  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
								  Ogre::TEX_TYPE_2D,
								  512,
								  512,
								  0,
								  Ogre::PF_R8G8B8,
								  Ogre::TU_RENDERTARGET);

  Ogre::RenderTexture* rtex = tex->getBuffer()->getRenderTarget();

  Ogre::Viewport* v = rtex->addViewport(_camBack);
  v->setOverlaysEnabled(false);
  v->setClearEveryFrame(true);
  v->setBackgroundColour(Ogre::ColourValue::Black);

  CEGUI::Texture& guiTex = renderer->createTexture(tex);
  CEGUI::Imageset& imageSet = CEGUI::ImagesetManager::getSingleton().create("RTTImageset", guiTex);
  imageSet.defineImage("RTTImage",
		       CEGUI::Point(0.0f,0.0f),
		       CEGUI::Size(guiTex.getSize().d_width, guiTex.getSize().d_height),
		       CEGUI::Point(0.0f,0.0f));

  

  CEGUI::Window* ex1 = CEGUI::WindowManager::getSingleton().loadWindowLayout("render.layout");


  //CEGUI::Window* camWin = CEGUI::WindowManager::getSingleton().getWindow("CamWin");
  CEGUI::Window* RTTWindow = CEGUI::WindowManager::getSingleton().getWindow("CamWin/RTTWindow");
  
  RTTWindow->setProperty("Image",CEGUI::PropertyHelper::imageToString(&imageSet.getImage("RTTImage")));


  //sheet->addChildWindow(camWin);

  //Quit button
  CEGUI::Window* exitButton = CEGUI::WindowManager::getSingleton().getWindow("ExitButton");
  exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
  			     CEGUI::Event::Subscriber(&MyFrameListener::quit, 
  						      _framelistener));
  //Attaching buttons
  sheet->addChildWindow(ex1);
  CEGUI::System::getSingleton().setGUISheet(sheet);
}
