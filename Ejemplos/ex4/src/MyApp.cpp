/*********************************************************************
 * Módulo 2. Curso de Experto en Desarrollo de Videojuegos
 * Autor: Carlos González Morcillo     Carlos.Gonzalez@uclm.es
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#include "MyApp.h" 

MyApp::MyApp() {
  _sceneManager = NULL;
  _framelistener = NULL;
}

MyApp::~MyApp() {
  //  delete _root;
  //  delete _framelistener;
}

int MyApp::start() {
  _root = new Ogre::Root();
  _root->showConfigDialog();
  if(!_root->restoreConfig()) {
    _root->showConfigDialog();
    _root->saveConfig();
  }
  
  Ogre::RenderWindow* window = _root->initialise(true,"MyApp Example");
  _sceneManager = _root->createSceneManager(Ogre::ST_GENERIC);
  
   Ogre::Camera* cam = _sceneManager->createCamera("MainCamera");
  cam->setPosition(Ogre::Vector3(5,20,20));
  cam->lookAt(Ogre::Vector3(0,0,0));
  cam->setNearClipDistance(5);
  cam->setFarClipDistance(10000);
  
  Ogre::Viewport* viewport = window->addViewport(cam);
  viewport->setBackgroundColour(Ogre::ColourValue(0.0,0.0,0.0));
  double width = viewport->getActualWidth();
  double height = viewport->getActualHeight();
  cam->setAspectRatio(width / height);
  
  
  loadResources();
  createScene();
  
  Ogre::SceneNode *node = _sceneManager->getSceneNode("SinbadNode");
  
  _framelistener = new MyFrameListener(window, cam, node);
  _root->addFrameListener(_framelistener);
  
  createGUI();

  _root->startRendering();

  return 0;
}

void MyApp::loadResources() {
  Ogre::ConfigFile cf;
  cf.load("resources2.cfg");
  
  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;    datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation
            (datastr, typestr, sectionstr);	
    }
  }
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void MyApp::createScene() {
  Ogre::Entity* ent1 = _sceneManager->createEntity("Sinbad.mesh");
  Ogre::SceneNode* node1 = _sceneManager->createSceneNode("SinbadNode");
  node1->attachObject(ent1);
  _sceneManager->getRootSceneNode()->addChild(node1);


}

void MyApp::createGUI()
{
  //CEGUI
  renderer = &CEGUI::OgreRenderer::bootstrapSystem();
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  CEGUI::SchemeManager::getSingleton().create("TaharezLook.scheme");
  CEGUI::System::getSingleton().setDefaultFont("DejaVuSans-10");
  CEGUI::System::getSingleton().setDefaultMouseCursor("TaharezLook","MouseArrow");

  //Sheet
  CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Sheet");

  //Config Window
  CEGUI::Window* formatWin = CEGUI::WindowManager::getSingleton().loadWindowLayout("stringFormat.layout");
  
  //Setting Text!
  CEGUI::WindowManager::getSingleton().getWindow("FormatWin/Text1")->setText("Este color es [colour='FFFF0000'] AZUL, mientras que [colour='FF00FF00'] este es ROJO [colour='FF0000FF'] y este VERDE!");
  CEGUI::WindowManager::getSingleton().getWindow("FormatWin/Text2")->setText("El tipo de letra puede [font='Batang-26']cambiar de un momento a otro, [font='fkp-16']y sin previo aviso!");
  CEGUI::WindowManager::getSingleton().getWindow("FormatWin/Text3")->setText("Si pulsas aqui [image-size='w:40 h:55'][image='set:TaharezLook image:CloseButtonNormal'] no pasar� nada :(");
  CEGUI::WindowManager::getSingleton().getWindow("FormatWin/Text4")->setText("[font='Batang-26'] Soy GRANDE,   [font='DejaVuSans-10'][vert-alignment='top'] puedo ir arriba,    [vert-alignment='bottom']o abajo,    [vert-alignment='centre']al centro... y para dentro!");
  CEGUI::WindowManager::getSingleton().getWindow("FormatWin/Text5")->setText("En un lugar de la [padding='l:20 t:15 r:20 b:15']Mancha[padding='l:0 t:0 r:0 b:0'], de cuyo nombre no quiero acordarme, no ha mucho...");
   
  //Exit Window
  CEGUI::Window* exitButton = CEGUI::WindowManager::getSingleton().getWindow("FormatWin/ExitButton");
  exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
			     CEGUI::Event::Subscriber(&MyFrameListener::quit, 
						      _framelistener));
  //Attaching buttons
  sheet->addChildWindow(formatWin);
  CEGUI::System::getSingleton().setGUISheet(sheet);
}
