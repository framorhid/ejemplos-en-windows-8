/*********************************************************************
 * Módulo 2. Curso de Experto en Desarrollo de Videojuegos
 * Autor: Carlos González Morcillo     Carlos.Gonzalez@uclm.es
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/
#include "MyFrameListener.h"

#include "Shapes/OgreBulletCollisionsBoxShape.h"
#include "Shapes/OgreBulletCollisionsCompoundShape.h"

#include "OgreBulletDynamicsWorld.h"
#include "OgreBulletDynamicsRigidBody.h"
#include "Debug/OgreBulletCollisionsDebugDrawer.h"

#include "Constraints/OgreBulletDynamicsRaycastVehicle.h"

using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

static float gWheelRadius = 0.5f;
static float gWheelWidth = 0.4f;
static float gWheelFriction = 1e30f;
static float gRollInfluence = 0.1f;
static float gSuspensionRestLength = 0.6;
static float gEngineForce = 1000.0;

// ----------------------------------------------------------------------
// MyFrameListener
// ----------------------------------------------------------------------

MyFrameListener::MyFrameListener(RenderWindow* win, 
				 Camera* cam, 
				 OverlayManager *om, 
				 SceneManager *sm) {
  OIS::ParamList param;
  size_t windowHandle;  std::ostringstream wHandleStr;

  _camera = cam;  _overlayManager = om; _sceneManager = sm;
  
  win->getCustomAttribute("WINDOW", &windowHandle);
  wHandleStr << windowHandle;
  param.insert(std::make_pair("WINDOW", wHandleStr.str()));
  
  _inputManager = OIS::InputManager::createInputSystem(param);
  _keyboard = static_cast<OIS::Keyboard*>
    (_inputManager->createInputObject(OIS::OISKeyboard, false));
  _mouse = static_cast<OIS::Mouse*>
    (_inputManager->createInputObject(OIS::OISMouse, false));
  _mouse->getMouseState().width = win->getWidth();
  _mouse->getMouseState().height = win->getHeight();

  // Creacion del modulo de debug visual de Bullet ------------------
  _debugDrawer = new OgreBulletCollisions::DebugDrawer();
  _debugDrawer->setDrawWireframe(true);	 
   SceneNode *node = _sceneManager->getRootSceneNode()->
    createChildSceneNode("debugNode", Vector3::ZERO);
  node->attachObject(static_cast <SimpleRenderable *>(_debugDrawer));

  // Creacion del mundo (definicion de los limites y la gravedad) ---
  AxisAlignedBox worldBounds = AxisAlignedBox (
    Vector3 (-100, -100, -100), 
    Vector3 (100,  100,  100));
  Vector3 gravity = Vector3(0, -9.8, 0);

  _world = new OgreBulletDynamics::DynamicsWorld(_sceneManager,
 	   worldBounds, gravity);
  _world->setDebugDrawer (_debugDrawer);

  // Creacion de los elementos iniciales del mundo
  CreateInitialWorld();
}

// ----------------------------------------------------------------------
// ~MyFrameListener
// ----------------------------------------------------------------------

MyFrameListener::~MyFrameListener() {
  _inputManager->destroyInputObject(_keyboard);
  _inputManager->destroyInputObject(_mouse);
  OIS::InputManager::destroyInputSystem(_inputManager);

  // Eliminar mundo dinamico y debugDrawer -------------------------
  delete _world->getDebugDrawer();    _world->setDebugDrawer(0);
  delete _world;
}

// ----------------------------------------------------------------------
// CreateInitialWorld
// ----------------------------------------------------------------------

void MyFrameListener::CreateInitialWorld() {
  // Creacion de la entidad y del SceneNode ------------------------
  Plane plane1(Vector3(0,1,0), 0);    // Normal y distancia
  MeshManager::getSingleton().createPlane("p1",
	ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1,
	200, 200, 1, 1, true, 1, 20, 20, Vector3::UNIT_Z);
  SceneNode* nodep = _sceneManager->createSceneNode("ground");
  Entity* groundEnt = _sceneManager->createEntity("planeEnt", "p1");
  groundEnt->setMaterialName("Ground");
  nodep->attachObject(groundEnt);
  _sceneManager->getRootSceneNode()->addChild(nodep);

  // Creamos forma de colision para el plano ----------------------- 
  OgreBulletCollisions::CollisionShape *Shape;
  Shape = new OgreBulletCollisions::StaticPlaneCollisionShape
    (Vector3(0,1,0), 0);   // Vector normal y distancia
  OgreBulletDynamics::RigidBody *rigidBodyPlane = new 
    OgreBulletDynamics::RigidBody("rigidBodyPlane", _world);

  // Creamos la forma estatica (forma, Restitucion, Friccion) ------
  rigidBodyPlane->setStaticShape(Shape, 0.1, 0.8); 
  
  // Creamos el vehiculo =============================================
  const Ogre::Vector3 chassisShift(0, 1.0, 0);
  float connectionHeight = 0.7f;
  mSteering = 0.0;
  
  mChassis = _sceneManager->createEntity("chassis", "chassis.mesh");
  SceneNode *node = _sceneManager->getRootSceneNode()->createChildSceneNode ();
  
  SceneNode *chassisnode = node->createChildSceneNode();
  chassisnode->attachObject (mChassis);
  chassisnode->setPosition (chassisShift);

  BoxCollisionShape* chassisShape = new BoxCollisionShape(Ogre::Vector3(1.f,0.75f,2.1f));
  CompoundCollisionShape* compound = new CompoundCollisionShape();
  compound->addChildShape(chassisShape, chassisShift); 

  mCarChassis = new WheeledRigidBody("carChassis", _world);

  Vector3 CarPosition = Vector3(0, 0, -15);
  mCarChassis->setShape (node, compound, 0.6, 0.6, 800, CarPosition, Quaternion::IDENTITY);
  mCarChassis->setDamping(0.2, 0.2);
  mCarChassis->disableDeactivation();

  mTuning = new VehicleTuning(20.2, 4.4, 2.3, 500.0, 10.5);
  mVehicleRayCaster = new VehicleRayCaster(_world);
  mVehicle = new RaycastVehicle(mCarChassis, mTuning, mVehicleRayCaster);

  mVehicle->setCoordinateSystem(0, 1, 2);

  Ogre::Vector3 wheelDirectionCS0(0,-1,0);
  Ogre::Vector3 wheelAxleCS(-1,0,0);

  for (size_t i = 0; i < 4; i++) {
    mWheels[i] = _sceneManager->createEntity("wheel" + i, "wheel.mesh");
    mWheels[i]->setCastShadows(true);

    mWheelNodes[i] = _sceneManager->getRootSceneNode()->createChildSceneNode();
    mWheelNodes[i]->attachObject (mWheels[i]);
  }

  bool isFrontWheel = true;
  Ogre::Vector3 connectionPointCS0 (1-(0.3*gWheelWidth),
		    connectionHeight, 2-gWheelRadius);

  mVehicle->addWheel(mWheelNodes[0], connectionPointCS0, wheelDirectionCS0, 
		     wheelAxleCS, gSuspensionRestLength, gWheelRadius,
		     isFrontWheel, gWheelFriction, gRollInfluence);

  connectionPointCS0 = Ogre::Vector3(-1+(0.3*gWheelWidth),
				     connectionHeight, 2-gWheelRadius);

  mVehicle->addWheel(mWheelNodes[1], connectionPointCS0,
		     wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength,
		     gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);


  isFrontWheel = false;
  connectionPointCS0 = Ogre::Vector3(-1+(0.3*gWheelWidth),
				     connectionHeight,-2+gWheelRadius);

  mVehicle->addWheel(mWheelNodes[2], connectionPointCS0,
		     wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength,
                    gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);

  connectionPointCS0 = Ogre::Vector3(1-(0.3*gWheelWidth),
				     connectionHeight,-2+gWheelRadius);

  mVehicle->addWheel(mWheelNodes[3], connectionPointCS0,
                    wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength,
                    gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);
}

// ----------------------------------------------------------------------
// Framestarted
// ----------------------------------------------------------------------

bool MyFrameListener::frameStarted(const Ogre::FrameEvent& evt) {
  Ogre::Vector3 vt(0,0,0);     Ogre::Real tSpeed = 20.0;  
  Ogre::Real deltaT = evt.timeSinceLastFrame;
  int fps = 1.0 / deltaT;
  bool mbleft, mbmiddle, mbright; // Botones del raton pulsados

  _world->stepSimulation(deltaT); // Actualizar simulacion Bullet

  _keyboard->capture();

  mVehicle->applyEngineForce (0,0); mVehicle->applyEngineForce (0,1); 

  if (_keyboard->isKeyDown(OIS::KC_ESCAPE)) return false;
  if (_keyboard->isKeyDown(OIS::KC_D)) _world->setShowDebugShapes (true); 
  if (_keyboard->isKeyDown(OIS::KC_H)) _world->setShowDebugShapes (false); 

  if (_keyboard->isKeyDown(OIS::KC_UP)) {
    mVehicle->applyEngineForce (gEngineForce, 0); 
    mVehicle->applyEngineForce (gEngineForce, 1); 
  }

  if (_keyboard->isKeyDown(OIS::KC_DOWN)) {
    mVehicle->applyEngineForce (-gEngineForce, 0); 
    mVehicle->applyEngineForce (-gEngineForce, 1); 
  }

  if (_keyboard->isKeyDown(OIS::KC_LEFT)) {
    if (mSteering < 0.8) mSteering+=0.01;
    mVehicle->setSteeringValue (mSteering, 0); 
    mVehicle->setSteeringValue (mSteering, 1); 
  }

  if (_keyboard->isKeyDown(OIS::KC_RIGHT)) {
    if (mSteering > -0.8) mSteering-=0.01;
    mVehicle->setSteeringValue (mSteering, 0); 
    mVehicle->setSteeringValue (mSteering, 1); 
  }

  int posx = _mouse->getMouseState().X.abs;   // Posicion del puntero
  int posy = _mouse->getMouseState().Y.abs;   //  en pixeles.

  _camera->moveRelative(vt * deltaT * tSpeed);
  if (_camera->getPosition().length() < 10.0) {
    _camera->moveRelative(-vt * deltaT * tSpeed);
  }

  _mouse->capture();

 // Si usamos la rueda, desplazamos en Z la camara ------------------
  vt+= Ogre::Vector3(0,0,-0.5)*deltaT * _mouse->getMouseState().Z.rel;   
  _camera->moveRelative(vt * deltaT * tSpeed);

  // Botones del raton pulsados? -------------------------------------
  mbleft = _mouse->getMouseState().buttonDown(OIS::MB_Left);
  mbmiddle = _mouse->getMouseState().buttonDown(OIS::MB_Middle);
  mbright = _mouse->getMouseState().buttonDown(OIS::MB_Right);

  if (mbmiddle) { // Con boton medio pulsado, rotamos camara ---------
    float rotx = _mouse->getMouseState().X.rel * deltaT * -1;
    float roty = _mouse->getMouseState().Y.rel * deltaT * -1;
    _camera->yaw(Ogre::Radian(rotx));
    _camera->pitch(Ogre::Radian(roty));
  }

  Ogre::OverlayElement *oe;
  oe = _overlayManager->getOverlayElement("cursor");
  oe->setLeft(posx);  oe->setTop(posy);

  oe = _overlayManager->getOverlayElement("fpsInfo");
  oe->setCaption(Ogre::StringConverter::toString(fps));

  return true;
}

// ----------------------------------------------------------------------
// frameEnded
// ----------------------------------------------------------------------

bool MyFrameListener::frameEnded(const Ogre::FrameEvent& evt) {
  Real deltaT = evt.timeSinceLastFrame;
  _world->stepSimulation(deltaT); // Actualizar simulacion Bullet
  return true;
}
