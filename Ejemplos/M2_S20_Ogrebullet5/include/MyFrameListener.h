#include <Ogre.h>
#include <OIS/OIS.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

using namespace Ogre;

class MyFrameListener : public Ogre::FrameListener {
private:
  Ogre::Camera* _camera;
  Ogre::OverlayManager* _overlayManager;
  Ogre::SceneManager *_sceneManager;

  OIS::InputManager* _inputManager;
  OIS::Keyboard* _keyboard;
  OIS::Mouse* _mouse;

  OgreBulletDynamics::DynamicsWorld * _world;
  OgreBulletCollisions::DebugDrawer * _debugDrawer;

  OgreBulletDynamics::WheeledRigidBody  *mCarChassis;
  OgreBulletDynamics::VehicleTuning     *mTuning;
  OgreBulletDynamics::VehicleRayCaster  *mVehicleRayCaster;
  OgreBulletDynamics::RaycastVehicle    *mVehicle;

  Ogre::Entity    *mChassis;
  Ogre::Entity    *mWheels[4];
  Ogre::SceneNode *mWheelNodes[4];
  
  float mSteering;

  void CreateInitialWorld();

public:
  MyFrameListener(Ogre::RenderWindow* win, Ogre::Camera* cam, 
		  Ogre::OverlayManager* om, Ogre::SceneManager* sm);
  ~MyFrameListener();
  bool frameStarted(const Ogre::FrameEvent& evt);  
  bool frameEnded(const Ogre::FrameEvent& evt);  
};
